%% Figure 7
%

calcResults = -1;
while calcResults ~= 1 && calcResults ~= 2
    calcResults = input(['Do you want to (1) Plot results from the' ...
        ' paper, or (2) Generate results and plot them:']);
end

%% Calculate results if required
if calcResults == 2
    
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure7'));
    
    addpath(p1,p2)
    
    fprintf('Running tests on %s:\n','gpu')
    
    RunTestsFigure7
    
    fprintf('Done.\n\n')
    
    rmpath(p1,p2)
    
end

%% Plot results
p1 = genpath(fullfile('NEAREST','lib'));
p2 = genpath(fullfile('generateResults','figure7'));

addpath(p1,p2)
load('spermParticleTracking.mat','px','py','pz','swimmer', ...
    'boundary','ts','zs','ux','uy','uz','xv1','xv2')

%% Plot
figure(6)
clf

%% Plot velocity profiles at 4 time points
for iT = 1 : numel(ts)
    subplot(3,2,iT)
    
    % Plot velocity field
    u = reshape(ux(:,iT),size(xv1));
    v = reshape(uy(:,iT),size(xv1));
    w = reshape(uz(:,iT),size(xv1));  
    vel = ( u.^2 + v.^2 + w.^2 ).^0.5;
    
    % If iT == 1 set vel to very small rather than 0 for log plot
    if iT == 1
        vel = vel + 1e-12;
    end
    
    pcolor(xv1,xv2,vel)
    shading interp
    colorbar
    hold on
    
    % Extract swimmer position
    xForce = GetSwimmerPoints(swimmer,zs(:,iT),ts(iT)*2*pi,'cpu');
    [xf1,xf2] = ExtractComponents(xForce);
    plot(xf1,xf2,'.w','markersize',6)
    
    % Plot particle position
    plot(px(:,iT),py(:,iT),'.w','markersize',6);        
    
    % Set colourscale logarithmic
    h = gca;
    h.ColorScale = 'log';
    
    axis([-1.5,4,-2,2])
    
    title(sprintf('t %f beats',ts(iT)))
end

%% Plot sketch
subplot(3,1,3)
xB = boundary.fn(boundary.model);
[xb1,xb2,xb3] = ExtractComponents(xB);
plot3(xb1,xb2,xb3,'k.')
hold on

xForce = GetSwimmerPoints(swimmer,zs(:,1),0,'cpu');
[xf1,xf2,xf3] = ExtractComponents(xForce);
plot3(xf1,xf2,xf3,'.r','markersize',12)

plot3(px(:,1),py(:,1),pz(:,1),'.r','markersize',12);
hold off
axis equal

%% Remove path
rmpath(p1,p2)
clearvars
