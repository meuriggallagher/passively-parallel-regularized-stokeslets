# Passively parallel regularized stokeslets
#
Code for producing results and figures for the paper:
Passively parallel regularized stokeslets
Meurig T. Gallagher and David J. Smith
2020

Each figure can be run by running the relevant file in MATLAB.