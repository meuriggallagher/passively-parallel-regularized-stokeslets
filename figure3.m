%% Figure 3
%

calcResults = -1;
while calcResults ~= 1 && calcResults ~= 2
    calcResults = input(['Do you want to (1) Plot results from the' ...
        ' paper, or (2) Generate results and plot them:']);
end

%% Calculate results if required
if calcResults == 2
    
    nTests = input('Please specify the number of tests to run:');
    
    p1 = genpath(fullfile('NEAREST','lib');
    p2 = genpath(fullfile('generateResults','figure3'));
    
    addpath(p1,p2)
    
    fprintf('Running %i tests on %s:\n',nTests,'gpu')
    
    RunTestsFigure3('gpu',nTests)
    
    fprintf('Done.\n\n')
    
    fprintf('Running %i tests on %s:\n',nTests,'cpu')
    
    RunTestsFigure3('cpu',nTests)
    
    fprintf('Done.\n\n')
    
    rmpath(p1,p2)
    
end

%% Plot results
if calcResults == 1

    p2 = genpath(fullfile('generateResults','figure3'));
    addpath(p2);
    
    load('fig3_bb_cpu.mat','bbtimeAScpu','bbtimeAcpu', ...
        'bbtimeBACKSLASHcpu','bbtimeTotalcpu','bbNcpu')
    load('fig3_bb_gpu.mat','bbtimeASgpu','bbtimeAgpu', ...
        'bbtimeBACKSLASHgpu','bbtimeTotalgpu','bbNgpu')
    load('fig3_ws_cpu.mat','wstimeAScpu','wstimeAcpu', ...
        'wstimeBACKSLASHcpu','wstimeTotalcpu','wsNcpu')
    load('fig3_ws_gpu.mat','wstimeASgpu','wstimeAgpu', ...
        'wstimeBACKSLASHgpu','wstimeTotalgpu','wsNgpu')
    
    rmpath(p2)
    
    figure(3)
    clf
    
    % AS time
    subplot(3,3,1)
    plot(3*(100 + 6*wsNcpu.^2),wstimeAScpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*wsNgpu.^2),wstimeASgpu,'b.-','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNcpu.^2),bbtimeAScpu,'rx--','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNgpu.^2),bbtimeASgpu,'b.--','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('AS')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % A time
    subplot(3,3,2)
    plot(3*(100 + 6*wsNcpu.^2),wstimeAcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*wsNgpu.^2),wstimeAgpu,'b.-','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNcpu.^2),bbtimeAcpu,'rx--','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNgpu.^2),bbtimeAgpu,'b.--','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('A')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % \ time
    subplot(3,3,3)
    plot(3*(100 + 6*wsNcpu.^2),wstimeBACKSLASHcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*wsNgpu.^2),wstimeBACKSLASHgpu,'b.-','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNcpu.^2),bbtimeBACKSLASHcpu,'rx--','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNgpu.^2),bbtimeBACKSLASHgpu,'b.--','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('BACKSLASH')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    
    % AS Speedup
    subplot(3,3,4)
    plot(3*(100 + 6*wsNcpu.^2),wstimeAScpu./wstimeASgpu,'bx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*bbNcpu.^2),bbtimeAScpu./bbtimeASgpu,'bx--','markersize',24,'linewidth',1)
    legend('ws','bb')
    hold off
    title('AS speedup')
    legend('ws','bb')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % A speedup
    subplot(3,3,5)
    plot(3*(100 + 6*wsNcpu.^2),wstimeAcpu./wstimeAgpu,'bx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*bbNcpu.^2),bbtimeAcpu./bbtimeAgpu,'bx--','markersize',24,'linewidth',1)
    legend('ws','bb')
    hold off
    title('A speedup')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % \ speedup
    subplot(3,3,6)
    plot(3*(100 + 6*wsNcpu.^2),wstimeBACKSLASHcpu./wstimeBACKSLASHgpu,'bx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*bbNcpu.^2),bbtimeBACKSLASHcpu./bbtimeBACKSLASHgpu,'bx--','markersize',24,'linewidth',1)
    legend('ws','bb')
    hold off
    title('BACKSLASH speedup')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % total time
    subplot(3,3,7)
    plot(3*(100 + 6*wsNcpu.^2),wstimeTotalcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*wsNgpu.^2),wstimeTotalgpu,'b.-','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNcpu.^2),bbtimeTotalcpu,'rx--','markersize',24,'linewidth',1)
    plot(3*(100 + 6*bbNgpu.^2),bbtimeTotalgpu,'b.--','markersize',24,'linewidth',1)
    hold off
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % total time speedup
    subplot(3,3,8)
    plot(3*(100 + 6*wsNcpu.^2),wstimeTotalcpu./wstimeTotalgpu,'bx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*bbNcpu.^2),bbtimeTotalcpu./bbtimeTotalgpu,'bx--','markersize',24,'linewidth',1)
    hold off
    title('speedup')
    legend('ws','bb')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
elseif calcResults == 2
    p2 = genpath('generateResults/figure3/');
    
    addpath(p2)
    
    load('fig3_cpu.mat','timeAScpu','timeAcpu', ...
        'timeBACKSLASHcpu','timeTotalcpu','Ncpu')
    load('fig3_gpu.mat','timeASgpu','timeAgpu', ...
        'timeBACKSLASHgpu','timeTotalgpu','Ngpu')
    
    rmpath(p2)
    
    figure(3)
    clf
    
    % AS time
    subplot(3,3,1)
    plot(3*(100 + 6*Ncpu.^2),timeAScpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*Ngpu.^2),timeASgpu,'b.-','markersize',24,'linewidth',1)
    legend('cpu','gpu')
    hold off
    title('AS')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % A time
    subplot(3,3,2)
    plot(3*(100 + 6*Ncpu.^2),timeAcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*Ngpu.^2),timeAgpu,'b.-','markersize',24,'linewidth',1)
    legend('cpu','gpu')
    hold off
    title('A')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % \ time
    subplot(3,3,3)
    plot(3*(100 + 6*Ncpu.^2),timeBACKSLASHcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*Ngpu.^2),timeBACKSLASHgpu,'b.-','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('BACKSLASH')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    
    % AS Speedup
    subplot(3,3,4)
    plot(3*(100 + 6*Ncpu.^2),timeAScpu./timeASgpu,'bx-','markersize',24,'linewidth',1)
    legend('cpu','gpu')
    title('AS speedup')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % A speedup
    subplot(3,3,5)
    plot(3*(100 + 6*Ncpu.^2),timeAcpu./timeAgpu,'bx-','markersize',24,'linewidth',1)
    title('A speedup')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % \ speedup
    subplot(3,3,6)
    plot(3*(100 + 6*Ncpu.^2),timeBACKSLASHcpu./timeBACKSLASHgpu,'bx-','markersize',24,'linewidth',1)
    title('BACKSLASH speedup')
    set(gca,'XScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % total time
    subplot(3,3,7)
    plot(3*(100 + 6*Ncpu.^2),timeTotalcpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(3*(100 + 6*Ngpu.^2),timeTotalgpu,'b.-','markersize',24,'linewidth',1)
    hold off
    legend('cpu','gpu')
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    % total time speedup
    subplot(3,3,8)
    plot(3*(100 + 6*Ncpu.^2),timeTotalcpu./timeTotalgpu,'bx-','markersize',24,'linewidth',1)
    title('speedup')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
end

