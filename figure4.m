%% Figure 4
%

calcResults = -1;
while calcResults ~= 1 && calcResults ~= 2
    calcResults = input(['Do you want to (1) Plot results from the' ...
        ' paper, or (2) Generate results and plot them:']);
end

%% Calculate results if required
if calcResults == 2
    
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure4'));
    
    addpath(p1,p2)
    
    fprintf('Running tests on %s:\n','gpu')
    
    RunTestsFigure4('gpu')
    
    fprintf('Done.\n\n')
    
    fprintf('Running tests on %s:\n','cpu')
    
    RunTestsFigure4('cpu')
    
    fprintf('Done.\n\n')
    
    rmpath(p1,p2)
    
end

%% Plot results
if calcResults == 1
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure4'));
    
    addpath(p1,p2)
    load('sperm_cpu_ws_6.mat')
    
    nSw = length(swimmer);
    
    %% Plot
    figure(4)
    clf
    %%
    subplot(3,2,1)
    tt = t(t < 2*pi);
    dt = 8;
    clrs = parula(11);
    iC = 0;
    dx = 1;
    dy = 1.5;
    np = swimmer{1}.model.ns + 6*swimmer{1}.model.nh^2;
    for ii = 1 : dt : length(tt)
        iC = iC + 1;
        
        xForce = GetSwimmerPoints(swimmer,z(ii,:),t(ii),'cpu');
        [x1,x2,x3]=ExtractComponents(xForce);
        
        iS = 4;
        x1 = x1(iS*np + 1:iS*np + np);
        x2 = x2(iS*np + 1:iS*np + np);
        x3 = x3(iS*np + 1:iS*np + np);
        
        x1 = x1 + dx*(mod(iC-1,3));
        
        if iC > 3 && iC < 7
            x2 = x2 - dy;
        elseif iC > 6
            x2 = x2 - 2*dy;
        end
        
        plot(x1,x2,'color',clrs(iC,:))
        hold on
        view(0,90)
        axis equal
        box on
    end
    axis([-2.25,1.25,-3.5,0.75])
    axis off
    
    
    %%
    subplot(3,2,2)
    clrs = parula(20);
    iC = 0;
    
    npts = (swimmer{1}.model.nh^2*6 + swimmer{1}.model.ns);
    for ii = 1
        iC = iC + 1;
        
        xForce = GetSwimmerPoints(swimmer,z(ii,:),t(ii),'cpu');
        [x1,x2,x3]=ExtractComponents(xForce);
        
        for iSw = 1 : nSw
            ind = npts*(iSw-1) + 1 : npts*iSw;
            
            plot(x1(ind),x2(ind),'color',clrs(iC,:))
            hold on
        end
        axis equal
        axis tight
        box on
        view(0,90)
        
    end
    
    hold on
    for ii = 1 : 9
        x0 = z(:,ii : 9 : ii + 2 * 9,:);
        
        plot(x0(:,1),x0(:,2),'color',clrs(16,:),'linewidth',2)
    end
    
    %%
    subplot(3,1,2)
    dt = 50;
    clrs = parula(20);
    iC = 0;
    for ii = 1 : dt :  length(t)
        iC = iC + 1;
        
        xForce = GetSwimmerPoints(swimmer,z(ii,:),t(ii),'cpu');
        [x1,x2,x3]=ExtractComponents(xForce);
        
        for iSw = 1 : nSw
            ind = npts*(iSw-1) + 1 : npts*iSw;
            
            plot3(x1(ind),x2(ind),x3(ind),'color',clrs(iC,:))
            hold on
        end
        hold on
        axis equal
        box on
    end
    
    hold on
    for ii = 1 : 9
        x0 = z(:,ii : 9 : ii + 2 * 9,:);
        
        plot3(x0(:,1),x0(:,2),x0(:,3),'color',clrs(16,:),'linewidth',2)
    end
    
    % plot boundary
    xb = boundary.fn(boundary.model);
    [x1,x2,x3] = ExtractComponents(xb);
    plot3(x1,x2,x3,'.','color',clrs(1,:),'markersize',24)
    view(-76.8628,1.7759)
    axis tight
    
    %%
    load('sperm_bnd_bb_cpu.mat','N','time_bb_cpu');
    bbNcpu = 3*(6*N.^2 + 40)*9 + 1440;
    
    load('sperm_bnd_bb_gpu.mat','N','time_bb_gpu');
    bbNgpu = 3*(6*N.^2 + 40)*9 + 1440;
    
    load('sperm_bnd_ws_cpu.mat','N','time_ws_cpu');
    wsNcpu = 3*(6*N.^2 + 40)*9 + 1440;
    
    load('sperm_bnd_ws_gpu.mat','N','time_ws_gpu');
    wsNgpu = 3*(6*N.^2 + 40)*9 + 1440;
    
    %%
    subplot(3,2,5)
    plot(wsNcpu,time_ws_cpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(wsNgpu,time_ws_gpu,'b.-','markersize',24,'linewidth',1)
    plot(bbNcpu,time_bb_cpu,'rx--','markersize',24,'linewidth',1)
    plot(bbNgpu,time_bb_gpu,'b.--','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    %%
    subplot(3,2,6)
    plot(wsNcpu,time_ws_cpu./time_ws_gpu(1:numel(time_ws_cpu)),'bx-','markersize',24,'linewidth',1)
    hold on
    plot(bbNcpu,time_bb_cpu./time_bb_gpu(1:numel(time_bb_cpu)),'bx--','markersize',24,'linewidth',1)
    hold off
    title('speedup')
    legend('ws','bb')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p1,p2)
    
elseif calcResults == 2
    
    p2 = genpath('generateResults/figure4/');
    
    addpath(p2)
    
    load('spermSimGpu.mat','N','timeSpermGPU');
    Ngpu = N * 25 * 3;
    
    load('spermSimCpu.mat','N','timeSpermCPU');
    Ncpu = N * 25 * 3;
    
    figure(4)
    clf
    subplot(1,2,1)
    plot(Ncpu,timeSpermCPU,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(Ngpu,timeSpermGPU,'b.-','markersize',24,'linewidth',1)
    legend('cpu','gpu')
    hold off
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    subplot(1,2,2)
    plot(Ncpu,timeSpermCPU./timeSpermGPU,'rx-','markersize',24,'linewidth',1)
    title('speedup')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p2)
end
