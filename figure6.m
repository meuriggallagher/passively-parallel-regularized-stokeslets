%% Figure 6
%

calcResults = -1;
while calcResults ~= 1 && calcResults ~= 2
    calcResults = input(['Do you want to (1) Plot results from the' ...
        ' paper, or (2) Generate results and plot them:']);
end

%% Calculate results if required
if calcResults == 2
    
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure6'));
    
    addpath(p1,p2)
    
    fprintf('Running tests on %s:\n','gpu')
    
    RunTestsFigure6('gpu')
    
    fprintf('Done.\n\n')
    
    fprintf('Running tests on %s:\n','cpu')
    
    RunTestsFigure6('cpu')
    
    fprintf('Done.\n\n')
    
    rmpath(p1,p2)
    
end

%% Plot results
if calcResults == 1
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure6'));
    
    addpath(p1,p2)
    load cElegans_gpu_bb_128_data.mat
    
    %% Update tangent angle function
    nSw = length(swimmer);
    c = 5.3; d = 3.1; freq = 1;
    for iSw = 1 : nSw
        swimmer{iSw}.model.tangentAngleFn = @(s,t) (pi*(d*s - c) ...
            .* sin(pi*(s - 2*t/freq)) + d*cos(pi*(s - 2*t/freq))) / pi^2 ...
            - (pi*(d*0 - c) * sin(pi*(0 - 2*t/freq)) ...
            + d*cos(pi*(0 - 2*t/freq))) / pi^2; %#ok<SAGROW>
    end
    
    %% Plot
    figure(6)
    clf
    %%
    subplot(2,2,1)
    tt = t(t < 1);
    dt = 16;
    clrs = parula(11);
    iC = 0;
    dx = 1;
    dy = 1.5;
    for ii = 1 : dt : length(tt)
        iC = iC + 1;
        
        xForce = GetSwimmerPoints(swimmer,z(ii,:),t(ii),'cpu');
        [x1,x2,x3]=ExtractComponents(xForce);
        
        iS = 8;
        x1 = x1(iS*128 + 1:iS*128 + 128);
        x2 = x2(iS*128 + 1:iS*128 + 128);
        x3 = x3(iS*128 + 1:iS*128 + 128);
        
        x1 = x1 + dx*(mod(iC-1,3));
        
        if iC > 3 && iC < 7
            x2 = x2 - dy;
        elseif iC > 6
            x2 = x2 - 2*dy;
        end
        
        plot3(x1,x2,x3,'color',clrs(iC,:),'linewidth',3)
        hold on
        view(0,90)
        axis equal
        box on
    end
    axis([-3.25,1.25,-2.25,2.25])
    axis off
    
    
    %%
    subplot(2,2,2)
    dt = 50;
    clrs = parula(20);
    iC = 0;
    for ii = 1
        iC = iC + 1;
        xForce = GetSwimmerPoints(swimmer,z(ii,:),t(ii),'cpu');
        [x1,x2,x3]=ExtractComponents(xForce);
        
        for iSw = 1 : nSw
            ind = 128*(iSw-1) + 1 : 128*iSw;
            
            plot3(x1(ind),x2(ind),x3(ind),'color',clrs(2,:))
            hold on
        end
        
        axis equal
        box on
        view(0,90)
    end
    
    hold on
    for ii = 1 : 25
        x0 = z(:,ii : 25 : ii + 2 * 25,:);
        
        plot3(x0(:,1),x0(:,2),x0(:,3),'color',clrs(14,:))
    end
    
    %%
    load('cElegansSimCpu_bb.mat','N','time_bb_cpu');
    bbNcpu = N * 25 * 3;
    
    load('cElegans_gpu_bb_128_data.mat','timeSim')
    
    load('cElegansSimGpu_bb.mat','N','time_bb_gpu');
    bbNgpu = N * 25 * 3;
    
    load('cElegansSimCpu_ws.mat','N','time_ws_cpu');
    wsNcpu = N * 25 * 3;
    
    load('cElegansSimGpu_ws.mat','N','time_ws_gpu');
    wsNgpu = N * 25 * 3;
    
    %%
    subplot(2,2,3)
    plot(wsNcpu,time_ws_cpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(wsNgpu,time_ws_gpu,'b.-','markersize',24,'linewidth',1)
    plot(bbNcpu,time_bb_cpu,'rx--','markersize',24,'linewidth',1)
    plot(bbNgpu,time_bb_gpu,'b.--','markersize',24,'linewidth',1)
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    hold off
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    %%
    subplot(2,2,4)
    plot(wsNcpu,time_ws_cpu./time_ws_gpu(1:numel(time_ws_cpu)),'bx-','markersize',24,'linewidth',1)
    hold on
    plot(bbNcpu,time_bb_cpu./time_bb_gpu(1:numel(time_bb_cpu)),'bx--','markersize',24,'linewidth',1)
    hold off
    title('speedup')
    legend('ws','bb')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p1,p2)
    
elseif calcResults == 2
    
    p2 = genpath(fullfile('generateResults','figure6'));
    
    addpath(p2)
    
    load('cElegansSimCpu.mat','N','timeCElegansGPU');
    Ngpu = N * 25 * 3;
    
    load('cElegansSimCpu.mat','N','timeCElegansCPU');
    Ncpu = N * 25 * 3;
    
    figure(6)
    clf
    subplot(1,2,1)
    plot(Ncpu,timeCElegansCPU,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(Ngpu,timeCElegansGPU,'b.-','markersize',24,'linewidth',1)
    legend('cpu','gpu')
    hold off
    title('total time')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('DOF')
    ylabel('time (s)')
    
    subplot(1,2,2)
    plot(Ncpu,timeCElegansCPU./timeCElegansGPU,'rx-','markersize',24,'linewidth',1)
    title('speedup')
    xlabel('DOF')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p2)
end
