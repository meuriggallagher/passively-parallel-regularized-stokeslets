function swimmer = InitialiseManySperm(NH,L0,epsilon,nBeats)

try
    load('manySpermSwimmers.mat','th0','k','phi0')
    
    nSperm = numel(th0);
catch
    nSperm = 9;
    
    % Get rotation thetas
    th0 = zeros(nSperm,1);
    
    % Get phase angles
    phi0 = zeros(nSperm,1);
    
    % Get wave numbers
    k = 2*pi * ones(nSperm,1);
    
    save('manySpermSwimmers.mat','th0','k','phi0');
end

bodySize = [2.0/L0, 1.6/L0, 1.0/L0];

nT = 40;
discrFnc = @(nH) [nT,nH,nT*4,nH*4];

discr = discrFnc(NH);

% Body frame angle
bodyFrame = cell(nSperm,1);
R = RotationMatrix(0,3);
for ii = 1 : nSperm
    bodyFrame{ii} = R;
end

% x0
ry = bodySize(2);
X0 = 0;
Y0 = (-4:4) * ry * 9;
Z0 = 0;

x0 = cell(nSperm,1);
for ii = 1 : nSperm
    x0{ii} = [X0 ; Y0(ii) ; Z0];
end

% Phase
args = cell(nSperm,1);
for ii = 1 : nSperm
    args{ii} = struct('phase',phi0(ii),'k',k(ii));
end

%% Construct swimmers
% All parameters except x0 the same so create one swimmer and copy
singleSwimmer = SwimmersSperm(1,@ModelSpermGI, ...
    @WaveSpermDKAct,bodySize,discr,nBeats, ...
    x0,bodyFrame,args);

swimmer = cell(nSperm,1);
for ii = 1 : nSperm
    swimmer{ii} = singleSwimmer{1};
    swimmer{ii}.x0 = x0{ii};
end

end
