%%Particle transport by a row of sperm swimming between two boundaries as
%shown in Gallagher & Smith 2020
%https://arxiv.org/abs/2001.06468
%
%Warning:
%This script may take a long time to run and requires the use of a GPU.
%

pLib = genpath('../../lib/');
addpath(pLib)

%% Setup
nBeats = 15;

% Length of sperm in um
L0 = 45; 

% Range of time to simulate over
tRange = [0, 2*pi*nBeats];

% Reguarlisation parameter
epsilon=0.25/L0;

% Infinite domain = reguliarised stokeslets
domain='i';

% Use the GPU to perform calculations
procFlag = 'gpu';

% To prevent too large calculations on GPU
blockSize = [4,0];

% boundary separation 10um depth
dpth = 10 / L0;

% Create boundary as two parallel plates
boundary = struct(            ...
    'fn',   @PlaneBoundary2,  ... 
    'model', struct(          ...
    'h',    dpth,             ...
    'nx',   51,               ...
    'ny',   51,               ...
    'Nx',   204,              ...
    'Ny',   204,              ...
    'Lx',   5.5,              ...
    'Ly',   4,                ...
    'O',    [1.25;0;-dpth/2]) ...
    );

% Initialise sperm
swimmer = InitialiseManySperm(3,L0,epsilon,nBeats);

%% Align swimmers in direction of the channel

R = RotationMatrix(-2.6535,3);
for ii = 1 : numel(swimmer)
    swimmer{ii}.b10 = R(:,1);
    swimmer{ii}.b20 = R(:,2);
end

%% NN matrix
NNMatrices = InitialiseNNMatrices('swimming',swimmer, ...
    boundary,blockSize);

%% Solve the swimming problem
fprintf('Beginning swimmer simulations...\n')

odeSolTimer = tic;
vargsIn = {swimmer,boundary,NNMatrices};
sol = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
    domain,blockSize,'ode45','swimming', ...
    procFlag,'f'); % 'f' to include forces

swimmerTime = toc(odeSolTimer);
fprintf('\tSwimmer simulations completed in %f seconds\n\n',swimmerTime)

%% Initialise particle paths
x0 = linspace(0.5,3,26);
y0 = linspace(-1.5,1.5,31);
z0 = 0;
[X0,Y0,Z0] = meshgrid(x0,y0,z0);

P0 = [X0(:) ; Y0(:) ; Z0(:)];

%% Solve particle tracks
fprintf('Beginning particle track simulations...\n')

pathSolTimer = tic;
pSol = ode45(@(T,P) SolveParticlePaths(T,P,sol,swimmer,boundary, ...
    epsilon,domain,blockSize,procFlag,NNMatrices{1}), ...
    tRange,P0);
particleTime = toc(pathSolTimer);

fprintf('\Particle transport simulations completed in %f seconds\n\n', ...
    particleTime)

%%
rmpath(pLib)
