function A = AssembleStokesletMatrixGpu(xCollNodes,xQuadNodes,N,eps, ...
    domain,blockSize,NN)

procFlag = 'gpu';

M=length(xCollNodes)/3;
Q=length(xQuadNodes)/3;

% calculate number of quadrature nodes that can be used for each block
blockNodes=floor(blockSize(1)*2^27/(9*M));

if blockSize(2) ~= 1
    A = gpuArray.zeros(3*M,3*N);
    
    for iMin=1:blockNodes:Q
        
        iMax=min(iMin+blockNodes-1,Q);
        iRange=[iMin:iMax Q+iMin:Q+iMax 2*Q+iMin:2*Q+iMax];
        
        gpuNN = gpuArray(NN(iRange,:));
        
        switch domain
            case 'i'
                A = A + RegStokeslet(xCollNodes,xQuadNodes(iRange), ...
                    eps,procFlag)*gpuNN;
            case 'h'
                A = A + RegBlakelet(xCollNodes,xQuadNodes(iRange), ...
                    eps)*gpuNN;
        end
        
        clearvars gpuNN
    end
else
    A = zeros(3*M,3*N);
    
    for iMin=1:blockNodes:Q
        
        iMax=min(iMin+blockNodes-1,Q);
        iRange=[iMin:iMax Q+iMin:Q+iMax 2*Q+iMin:2*Q+iMax];
        
        switch domain
            case 'i'
                S = RegStokeslet(xCollNodes,xQuadNodes(iRange),eps,procFlag);
            case 'h'
                S = RegBlakelet(xCollNodes,xQuadNodes(iRange),eps);
        end
        
        A = A + gather(S) * NN(iRange,:);
        clearvars gpuNN S
    end
    
    A = gpuArray(A);
end

end
