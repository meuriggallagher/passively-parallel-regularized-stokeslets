function z0 = InitCondition(swimmer)

nSw = length(swimmer);

% Calculate DOF
DOF = 0;
for iSw = 1 : nSw
    [xForce,~,~] = swimmer{iSw}.fn(0,swimmer{iSw}.model);
    DOF = DOF + length(xForce);
end

% Initial condition
z0 = zeros(9*nSw + DOF, 1);
for iSw = 1 : nSw
    z0(iSw)         = swimmer{iSw}.x0(1);
    z0(nSw   + iSw) = swimmer{iSw}.x0(2);
    z0(2*nSw + iSw) = swimmer{iSw}.x0(3);
    z0(3*nSw + iSw) = swimmer{iSw}.b10(1);
    z0(4*nSw + iSw) = swimmer{iSw}.b10(2);
    z0(5*nSw + iSw) = swimmer{iSw}.b10(3);
    z0(6*nSw + iSw) = swimmer{iSw}.b20(1);
    z0(7*nSw + iSw) = swimmer{iSw}.b20(2);
    z0(8*nSw + iSw) = swimmer{iSw}.b20(3);
end

end
