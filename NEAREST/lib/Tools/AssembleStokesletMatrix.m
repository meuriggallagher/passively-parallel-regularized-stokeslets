function [A,NN]=AssembleStokesletMatrix(xCollNodes,xQuadNodes, ...
    xForceNodes,eps,domain,blockSize,procFlag,varargin)

% blockSize is in GB
% stokeslet values are calculated in blocks to avoid memory overflow
% each block is multiplied by the nearest neighbour matrix
% the result is summed to yield the stokeslet matrix

if ~isempty(varargin)
    % supplied nearest-neighbour matrix
    NN=varargin{1};
else
    % find closest force node to each quadrature node
    NN=NearestNeighbourMatrix(xQuadNodes,xForceNodes, ...
        procFlag,blockSize);
end

M=length(xCollNodes)/3;
N=length(xForceNodes)/3;
Q=length(xQuadNodes)/3;

%assemble stokeslet matrix
switch procFlag
    case 'cpu'
        
        A = AssembleStokesletMatrixCpu(xCollNodes,xQuadNodes,N,eps, ...
            domain,blockSize,NN);
        
    case 'gpu'
        try
            % Try compiling entirely on GPU first then move to hybrid if
            % needed
            A = AssembleStokesletMatrixGpu(xCollNodes,xQuadNodes,N,eps, ...
                domain,blockSize,NN);
            
        catch
            % Change to hybrid calculation
            blockSize(2) = 1;
            
            A = AssembleStokesletMatrixGpu(xCollNodes,xQuadNodes,N,eps, ...
                domain,blockSize,NN);
            
        end
end

end
