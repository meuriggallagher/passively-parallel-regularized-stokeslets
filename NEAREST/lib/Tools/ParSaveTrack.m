function ParSaveTrack(saveName,saveDir,particleTracks)

successfulSave = 0;

while successfulSave == 0
    try
        % Get list of files
        nDirs = dir(fullfile(saveDir,'/*'));
        dirFlags = [nDirs.isdir] & ~strcmp({nDirs.name},'.') ...
            & ~strcmp({nDirs.name},'..');
        nDirs = length(nDirs(dirFlags));
        
        [success,msg,~] = mkdir(fullfile(saveDir,sprintf('%i',nDirs+1)));
        
        if strcmp(msg,'Directory already exists.') || success == 0
            
        else
            saveName = fullfile(saveDir,sprintf('%i',nDirs+1), ...
                saveName);
            
            save(saveName,'particleTracks');
            
            successfulSave = 1;
            
        end
        
    catch
    end
end

end
