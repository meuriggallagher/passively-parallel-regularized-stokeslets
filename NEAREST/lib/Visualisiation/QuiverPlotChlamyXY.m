
clf
pcolor(Xg,Yg,reshape((Ug.^2+Vg.^2+Wg.^2).^0.5,size(Xg)))
shading interp
colormap(parula(256))

hold on
uu = reshape(Ug,size(Xg));
vv = reshape(Vg,size(Xg));

[sx,sy] = meshgrid([-1.5:0.1:-0.1,0.1:0.1:1.5],2);
H = streamline(Xg',Yg',uu',vv',sx,sy);
set(H,'color','white','linewidth',2)

[sx,sy] = meshgrid([-1.5:0.75:-0.1,0.1:0.75:1.5],2);
H = streamline(Xg',Yg',-uu',-vv',sx,sy);
set(H,'color','white','linewidth',2)

[sx,sy] = meshgrid(-1.5,-1:0.25:2);
H = streamline(Xg',Yg',-uu',-vv',sx,sy);
set(H,'color','white','linewidth',2)

[sx,sy] = meshgrid(1.5,-1:0.25:2);
H = streamline(Xg',Yg',-uu',-vv',sx,sy);
set(H,'color','white','linewidth',2)

dx = Xg(1:5:end,1:5:end);
dy = Yg(1:5:end,1:5:end);
du = uu(1:5:end,1:5:end);
dv = vv(1:5:end,1:5:end);
quiver(dx(:),dy(:),4*du(:),4*dv(:),0,'color',[1,1,1]);%,[0.32054,0.76027,0.5]);
% h = [1,1];