function varargout = SolveSwimmingProblem(tRange, ...
    swimmer,boundary,epsilon,domain,blockSize,solveFlag, ...
    procFlag,NNMatrices,varargin)

nSw = length(swimmer);

% Calculate DOF
DOF = 0;
for iSw = 1 : nSw
    [xForce,~,~] = swimmer{iSw}.fn(0,swimmer{iSw}.model);
    DOF = DOF + length(xForce);
end

if ~isempty(boundary)
    [xQuad,~] = boundary.fn(boundary.model);
    DOF = DOF + length(xQuad);
end

fprintf(['Solving time-dependent swimming ', ...
    'problem with %i DOF...\n'],DOF)

z0 = zeros(9*nSw + DOF, 1);
for iSw = 1 : nSw
    z0(iSw)         = swimmer{iSw}.x0(1);
    z0(nSw   + iSw) = swimmer{iSw}.x0(2);
    z0(2*nSw + iSw) = swimmer{iSw}.x0(3);
    z0(3*nSw + iSw) = swimmer{iSw}.b10(1);
    z0(4*nSw + iSw) = swimmer{iSw}.b10(2);
    z0(5*nSw + iSw) = swimmer{iSw}.b10(3);
    z0(6*nSw + iSw) = swimmer{iSw}.b20(1);
    z0(7*nSw + iSw) = swimmer{iSw}.b20(2);
    z0(8*nSw + iSw) = swimmer{iSw}.b20(3);
end

if isempty(NNMatrices)
    NN = [];
    NNSw = [];
else
    NN = NNMatrices{1};
    NNSw = NNMatrices{2};
end

if isempty(varargin)
    keepForces = [];
else
    keepForces = 1;
end


switch solveFlag
    case 'ode45'
        if nargout == 1
            varargout{1} = ode45(@(t,z) SwimmingProblem(z,swimmer, ...
                boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
            
        else
            [varargout{1},varargout{2}]=ode45(@(t,z) SwimmingProblem( ...
                z,swimmer,boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
        end
        
    case 'ode113'
        if nargout == 1
            varargout{1} = ode113(@(t,z) SwimmingProblem(z,swimmer, ...
                boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
            
        else
            [varargout{1},varargout{2}]=ode113(@(t,z) SwimmingProblem( ...
                z,swimmer,boundary,t,epsilon,domain,blockSize,NN,NNSw, ...
                procFlag,keepForces), ...
                tRange,z0);
        end
        
    otherwise
        error('Solve flag not known')
end

end