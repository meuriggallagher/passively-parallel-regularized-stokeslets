% Constructs the force-free swimming problem for translational and angular
% velocity U, Om, in the presence of a stationary rigid boundary
% (e.g. finite plane wall)
%
% input: z{s}     - position/orientation of swimmer{s}
%                   z{s}(1:3) = x0   - origin of swimmer
%                   z{s}(4:6) = b1   - first basis vector of swimmer frame
%                   z{s}(7:9) = b2   - second basis vector of swimmer frame
%        swimmer  - structure describing how to construct swimmer
%        t        - time (scalar)
%        varargin - Nearest neighbour matrices:
%        varargin{1} - NN swimmer
%                    - NN boundary
%                    - full NN
%
%
% output: A, b     - matrix system A * x = b
%                     	where x comprises forces at all points x1 ; x2 ; x3
%                     	followed by velocities U in same order
%                       followed by angular velocities Om in same order
function dz = SwimmingProblem(z,swimmer,boundary,t,epsilon, ...
    domain, blockSize, NN, NNsw,procFlag,varargin)
%% Establish number of swimmers
nSw = length(swimmer);
t

%% Obtain swimmer points
[xForce,vForce,xQuad,xForceSwRotate,indSw,b1,b2] ...
    = GetSwimmerPoints(swimmer,z,t,'cpu');

% If NN is empty then calculate NN matrix
if isempty(NN)
    NNMatrices = InitialiseNNMatrices('swimming',swimmer, ...
        boundary,blockSize,t);
    NN = NNMatrices{1};
    NNsw = NNMatrices{2};
end


%% Extract boundary points
if ~isempty(boundary)
    [xForceBnd,xQuadBnd] = boundary.fn(boundary.model);
    vBnd = 0 * xForceBnd;
else
    xForceBnd = [];
    xQuadBnd = [];
    vBnd = [];
end

% Merge swimmer and boundary points
x = MergeVectorGrids(xForce, xForceBnd);
X = MergeVectorGrids(xQuad, xQuadBnd);
v = MergeVectorGrids(vForce, vBnd);

%% Assemble matrix system - mobility problem
NSw = length(xForce)/3;
QSw = length(xQuad)/3;
NBnd = length(xForceBnd)/3;
N = NSw + NBnd;

%% Assemble stokeslet matrix
switch procFlag
    case 'gpu'
        xF = gpuArray(x);
        xQ = gpuArray(X);
        
        [AS,~]=AssembleStokesletMatrix(xF,xQ,xF,epsilon, ...
            domain,blockSize,procFlag,NN);
    case 'cpu'
        [AS,~]=AssembleStokesletMatrix(x,X,x,epsilon, ...
            domain,blockSize,procFlag,NN);
end

%% AU block
% component of velocity due to translational velocity of swimmers;
%   zero velocity of boundary
au = zeros(NSw+NBnd,nSw);
for n = 1 : nSw
    au(indSw(n):indSw(n+1)-1,n) = -ones(indSw(n+1)-indSw(n),1);
end
AU = kron(eye(3),au);

%% AF - force summation - only on swimmer
af = zeros(nSw, N);
for n= 1 : nSw
    af(n,indSw(n):indSw(n+1)-1) = sum(NNsw(1:QSw,indSw(n):indSw(n+1)-1),1);
end
AF = kron(eye(3),af);

%% A0m - component of velocity due to rotation of swimmer about x0;
%   zero velocity of boundary
Ze  = zeros(N, nSw);
x1m = zeros(N, nSw);
x2m = zeros(N, nSw);
x3m = zeros(N, nSw);
for n = 1 : nSw
    [x1,x2,x3]=ExtractComponents(xForceSwRotate{n});
    x1m(indSw(n):indSw(n+1)-1,n) = x1;
    x2m(indSw(n):indSw(n+1)-1,n) = x2;
    x3m(indSw(n):indSw(n+1)-1,n) = x3;
end
AOm = [ Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];

%% AM
Ze  = zeros(nSw,N);
x1m = zeros(nSw,N);
x2m = zeros(nSw,N);
x3m = zeros(nSw,N);
[x1,x2,x3]=ExtractComponents(xQuad'*NNsw); % moment summation
for n=1:nSw
    x1m(n,indSw(n):indSw(n+1)-1)=x1(indSw(n):indSw(n+1)-1);
    x2m(n,indSw(n):indSw(n+1)-1)=x2(indSw(n):indSw(n+1)-1);
    x3m(n,indSw(n):indSw(n+1)-1)=x3(indSw(n):indSw(n+1)-1);
end
AM = [Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];

%% A
A=[ AS, AU, AOm            ; ...
    AF, zeros(3*nSw,6*nSw) ; ...
    AM, zeros(3*nSw,6*nSw)];

%% Assemble RHS of problem
b = [v ; zeros(6*nSw,1)];

%% Solve problem
sol = A \ b;

switch procFlag
    case 'gpu'
        sol = gather(sol);
end

%% Extract solution
% Initialise output
if isempty(varargin)
    dz = NaN(9*nSw, 1);
else
    dz = NaN(9*nSw+3*N, 1);
end

% Velocities
U = sol(3*N+1       : 3*N+3*nSw);
dz(1 : 3*nSw, 1)  = U;

% Angular velocities
Om= sol(3*N+3*nSw+1 : 3*N+6*nSw);
for n = 1 : nSw
    om = Om(n : nSw : n + 2*nSw);
    dz(3*nSw+n:nSw:n+5*nSw,1)  = cross(om, b1{n});
    dz(6*nSw+n:nSw:n+8*nSw,1)  = cross(om, b2{n});
end

% Forces
if ~isempty(varargin)
    f = sol(1 : 3*N);
    dz(9*nSw+1:9*nSw+3*N,1) = f;
end

end
