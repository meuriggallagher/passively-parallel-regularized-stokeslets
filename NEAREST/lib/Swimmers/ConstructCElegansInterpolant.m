function F = ConstructCElegansInterpolant(theta)

nS = 200;
s = linspace(0,1,2*nS+1);
ds = s(3)-s(1);
iO = 1:nS;     iO = [iO  , iO, iO  ];
jO = 2*(1:nS); jO = [jO-1, jO, jO+1];
aO = [1/6*ones(1,nS), 2/3*ones(1,nS), 1/6*ones(1,nS)];
A = ds*sparse(iO,jO,aO,nS,2*nS+1);
     
X = @(t) [0 ; cumsum(A*cos(theta(s,t)'))];

% Integrate sin(th) w.r.t. s to find Y position
Y = @(t) [0 ; cumsum(A*sin(theta(s,t)'))];

% Set up time
nT = 100;
t = linspace(0,1,nT+1); t = t(1:end-1);

x = zeros(nS+1,nT);
y = zeros(nS+1,nT);

for ii = 1 : nT
    dx = X(t(ii));
    dy = Y(t(ii));
    
    x(:,ii) = dx(:);
    y(:,ii) = dy(:);
end

s = linspace(0,1,nS+1);
[s,t] = ndgrid(s,t);
F{1} = griddedInterpolant(s,t,x,'spline');
F{2} = griddedInterpolant(s,t,y,'spline');


end