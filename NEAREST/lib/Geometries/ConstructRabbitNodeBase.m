function x = ConstructRabbitNodeBase(model,nPts)

% Create dense array of points
NGrid = 300; %Refining this makes more quad points
[x,y,z] = meshgrid(linspace(-15,15,NGrid), ...
    linspace(-15,15,NGrid),linspace(0,4,NGrid));

% Function for calculating level set
X = x/model.lxC;
Y = y/model.lyC;
Z = z/model.lzC;

f = X.^2 + Y.^2 + Z.^2 - 1;

% Calculate isosurface
isoval = 0;
[~,verts,~] = isosurface(x,y,z,f,isoval,0*f);

% Reduce number of points to get desired value
minFnc = @(tol) (size(uniquetol(verts,tol,'ByRows',true),1) ...
    - nPts).^2;

reqTol = fminbnd(minFnc,0,1);

x = uniquetol(verts,reqTol,'ByRows',true);

x = x(:);

end
