%GENERATEMOVINGBOUNDARY
%
function [x,v,R,NNMov] = GenerateMovingBoundary(t,model,varargin)

nCilia = model.nCilia;
runType = model.runType;
model = model.model;

switch runType
    case 'hybridStokeslets'
        x = []; v = []; R = [];
        for ii = 1:nCilia
            [x,v,R] = PlaceCilium(x,v,R,t,model(ii),runType);
        end
        
        % No NN matrix required
        NNMov = [];
        
    case 'nearestNeighbour'
        if ~isempty(varargin)
            x = []; v = []; R = []; NNMov = [];
            for ii = 1:nCilia
                [x,v,R,NNMov] = PlaceCilium(x,v,R,t,model(ii), ...
                    runType,NNMov);
            end
        else
            x = []; v = []; R = [];
            for ii = 1:nCilia
                [x,v,R] = PlaceCilium(x,v,R,t,model(ii), ...
                    runType);
            end
        end
end

%% Rearrange rotation matrices so they're in [x1 , x2 , x3] order
RX1 = R(:,1 : 3 : end);
RX2 = R(:,2 : 3 : end);
RX3 = R(:,3 : 3 : end);

R = [RX1,RX2,RX3];

end