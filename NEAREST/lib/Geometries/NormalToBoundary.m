function n = NormalToBoundary(x0,model)

switch model.animal
    case 'mouse'
        
        % Calculate normal to surface at [x0(1),x0(2),x0(3)]
        n = [ 2 * x0(1) / (model.lT^2), ...
            x0(2)^3 / (model.lT^4) - 3 * x0(2)^2 / (model.lT^3) ...
            + x0(2) * x0(3)^2/2/(model.lB^2 * model.lT^2) ...
            + 3/2*x0(2)/(model.lT^2) ...
            - x0(3)^2/(model.lB^2 * model.lT) + 1/model.lT, ...
            x0(2)^2 * x0(3)/2/(model.lB^2 * model.lT^2) ...
            - 2*x0(2)*x0(3)/(model.lB^2 * model.lT) ...
            + 2*x0(3)/(model.lB^2)];
        n = n / norm(n);
        
        % Ensure n pointing upwards
        if n(3) < 0
            n = -n;
        end
        
    case 'flatMouse'
        
        if x0(3) == 0
            n = [0, 0, 1];
        else
            % Calculate normal to surface at [x0(1),x0(2),x0(3)]
            n = [ 2 * x0(1) / (model.lT^2), ...
                x0(2)^3 / (model.lT^4) - 3 * x0(2)^2 / (model.lT^3) ...
                + x0(2) * x0(3)^2/2/(model.lB^2 * model.lT^2) ...
                + 3/2*x0(2)/(model.lT^2) ...
                - x0(3)^2/(model.lB^2 * model.lT) + 1/model.lT, ...
                x0(2)^2 * x0(3)/2/(model.lB^2 * model.lT^2) ...
                - 2*x0(2)*x0(3)/(model.lB^2 * model.lT) ...
                + 2*x0(3)/(model.lB^2)];
            n = n / norm(n);
            
            % Ensure n pointing upwards
            if n(3) < 0
                n = -n;
            end
        end
        
    case 'rabbit'
        
        % Calculate normal to surface at [x0(1),x0(2),x0(3)]
        n = [ 2*x0(1) / model.lxC^2, ...
            2*x0(2) / model.lyC^2, ...
            2*x0(3) / model.lzC^2];
        
        n = n / norm(n);
        
        % Ensure n pointing upwards
        if n(3) < 0
            n = -n;
        end
        
    otherwise
        error('Animal node boundary not specified')
end

end
