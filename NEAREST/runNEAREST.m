addpath(genpath('./'))

problemFlag = 'swimming';
swimmerType = 'celegans';
boundaryType = 'none';
procFlag = 'cpu';
blockSize = 0.2;


%% Swimmer types
switch problemFlag
    case 'swimming'
        switch swimmerType
            case 'sperm'
                nSperm = 2;
                nBeats = 3;
                L0 = 45; %um
                bodySize = [2.0/L0, 1.6/L0, 1.0/L0];
                discr = [40,4,100,10];
                
                x0 = cell(nSperm,1);
                x0{1} = [ 0;   0; 0.2];
                x0{2} = [-1; 0.5; 0.2];
                
                bodyFrame = cell(nSperm,1);
                bodyFrame{1} = RotationMatrix(0,3);
                bodyFrame{2} = RotationMatrix(0,3);
                
                args = cell(nSperm,1);
                args{1} = struct('phase',pi/5,'k',2*pi);
                args{2} = struct('phase',pi/5,'k',2*pi);
                
                swimmer = SwimmersSperm(nSperm,@ModelSpermGI, ...
                    @WaveSpermDKAct,bodySize,discr,nBeats, ...
                    x0,bodyFrame,args);
                
                tRange = [0, 2*pi*nBeats];
                epsilon=0.25/45;
                domain='i';
                
            case 'chlamy'
                nChlamy = 1;
                nBeats = 3;
                bodySize = [0.5,0.6,0.6];
                discr = [40,4,400,10];
                flagellarAngle = pi/5;
                
                % Initial position
                x00{1} = [0; 0; 0];
                
                % Initial body frame
                B{1} = RotationMatrix(pi/3,3);
                
                swimmer = InitialiseSwimmer(swimmerType,nChlamy, ...
                    @ModelChlamyMTG, @WaveChlamy,bodySize, ...
                    discr, nBeats, x00, B, flagellarAngle);
                
                
                tRange = [0, 2*pi*nBeats];
                epsilon=0.25/20;
                domain='i';
                
            case 'celegans'
                
                ncElegans = 1;
                nBeats = 1;
                bodySize = [35e-3,0.25,0.3];
                discr = [40,8,40*4,8*4];
                x0 = [-8.3551e-2; 4.4224e-1; 0];
                bodyFrame = RotationMatrix(pi/2-1.9382,3);
                
                tRange = [0, nBeats];
                epsilon=1e-3;
                domain='i';
                
                swimmer = SwimmersCElegans(ncElegans,@ModelCElegans, ...
                    bodySize,discr,x0,bodyFrame,epsilon);
                
            otherwise
                error('Swimming type not known')
        end
        
    otherwise
        error('Problem type not known')
end

%% Initialise boundary
switch boundaryType
    case 'none'
        
        boundary = [];
        
    case 'twoPlates'
        
        boundary = struct( ...
            'fn',    @PlaneBoundary2, ...
            'model', struct( ...
            'h',  0.4, ...
            'nx', 16, ...
            'ny', 15, ...
            'Nx', 32, ...
            'Ny', 30, ...
            'Lx', 3, ...
            'Ly', 3, ...
            'O',  [0,0,0]));
        
    case 'chamber'
        boundary = struct('fn', @ChamberEdgesBoundary, ...
            'model', struct( ...
            'hx', [], ...
            'hy', [], ...
            'hz', [], ...
            ...
            'qx', [], ...
            'qy', [], ...
            'qz', [], ...
            ...
            'Lx', bnd(2), ... % [10mm, 2.5mm, 1.8mm, 1.3mm, 0.8mm]
            'Ly', bnd(3), ... % 'inf' length relative to swimmer
            'Lz', bnd(4), ... % Fixed depth chamber
            ...
            'O', [0,0,0]));
        
        % Choose number of boundary points to fix step sizes
        hx = bnd(1);
        
        boundary.model.hx = ceil(boundary.model.Lx/hx);
        boundary.model.hy = ceil(boundary.model.Ly/hx);
        boundary.model.hz = ceil(boundary.model.Lz/hx);
        
        qx = hx/4;
        boundary.model.qx = ceil(boundary.model.Lx/qx);
        boundary.model.qy = ceil(boundary.model.Ly/qx);
        boundary.model.qz = ceil(boundary.model.Lz/qx);
        
    otherwise
        error('Boundary type not known')
end

%% Nearest-Neighbour matricies
NNMatrices = InitialiseNNMatrices(problemFlag,swimmer,boundary, ...
    blockSize);

%% Solve Nearest problem
tic
fprintf('starting solver\n')

vargsIn = {swimmer,boundary,NNMatrices};
[t,z] = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
    domain,blockSize,'ode45','swimming', ...
    procFlag);

solveTime = toc;
fprintf('CPU time taken = %f\n',solveTime)
