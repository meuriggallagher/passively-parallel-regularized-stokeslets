% Figure 1
%
p1 = genpath(fullfile('NEAREST','lib'));
addpath(p1)

%%

a=1;
nth=320;
nphi=500;
epsilon=0.1;
n=4;
q=12;

wd=5;
hh=5;

[x1,x2,x3]=GenerateSphereSurfaceForVisualisation(nth,nphi,a);

figure(1);
clf;
subplot(131)
hold on;
surf(x1,x2,x3,sqrt(x1.^2+x2.^2));shading flat;
[xn,xq]=GenerateSpherePoints(n,q,a*1.015);
[xn1,xn2,xn3]=ExtractComponents(xn);
[xq1,xq2,xq3]=ExtractComponents(xq);
np=50;
plot3(xn1,xn2,xn3,'k.','markersize',8)
axis equal;
view([-180 -90]);
axis off;

%%
kernel=@(r) 1./(r.^2+epsilon^2);

subplot(132)
hold on
surf(x1,x2,x3,kernel(sqrt((x1-xn1(np)).^2+(x2-xn2(np)).^2+(x3-xn3(np)).^2)))
shading flat;
plot3(xn1,xn2,xn3,'k.','markersize',2);
axis equal;
view([-180 -90]);
axis off;
plot3(xq1,xq2,xq3,'k.','markersize',2);

%%
subplot(133)
hold on;
surf(x1,x2,x3,kernel(sqrt((x1-xn1(np)).^2+(x2-xn2(np)).^2+(x3-xn3(np)).^2)));
shading flat;
plot3(xn1,xn2,xn3,'k.','markersize',2);
axis equal;
view([-180 -90]);
axis off;
plot3(xq1,xq2,xq3,'rx','markersize',1);
plot3(xn1,xn2,xn3,'ko','markersize',2);



%%
rmpath(p1)