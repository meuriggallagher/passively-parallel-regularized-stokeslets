%% Figure 5
%

calcResults = -1;
while calcResults ~= 1 && calcResults ~= 2
    calcResults = input(['Do you want to (1) Plot results from the' ...
        ' paper, or (2) Generate results and plot them ' ...
        '(this will take days of computational time):']);
end

%% Calculate results if required
if calcResults == 2
    
    p1 = genpath(fullfile('NEAREST','lib'));
    p2 = genpath(fullfile('generateResults','figure5'));
    
    addpath(p1,p2)
    
    fprintf('Running tests:\n')
    
    RunTestsFigure5
    
    fprintf('Done.\n\n')
    
    rmpath(p1,p2)
    
end

%% Plot results
if calcResults == 1
    p2 = genpath(fullfile('generateResults','figure5'));
    
    addpath(p2)
    
    %% Plot
    figure(5)
    clf
    
    %%
    load('bb_nodeTests_cpu_0_1.mat','simTime')
    bb_cpu = simTime(1:3);
    
    load('bb_nodeTests_cpu_100.mat','simTime')
    bb_cpu(3) = simTime;
    
    load('bb_nodeTests_gpu_0_1.mat','simTime')
    bb_gpu = simTime(1:3);
    
    load('bb_nodeTests_gpu_100.mat')
    bb_gpu(3) = simTime;
    
    ws_cpu = NaN(3,1);
    load('ws_nodeTests_cpu_1.mat')
    ws_cpu(1) = simTime;
    
    load('ws_nodeTests_cpu_10.mat')
    ws_cpu(2) = simTime;
    
    load('ws_nodeTests_cpu_100.mat')
    ws_cpu(3) = simTime;
    
    ws_gpu = NaN(3,1);
    load('ws_nodeTests_gpu_1.mat')
    ws_gpu(1) = simTime;
    
    load('ws_nodeTests_gpu_10.mat')
    ws_gpu(2) = simTime;
    
    load('ws_nodeTests_gpu_100.mat')
    ws_gpu(3) = simTime;
    
    N = [1,10,100];
    
    %%
    subplot(1,2,1)
    plot(N,ws_cpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(N,ws_gpu,'b.-','markersize',24,'linewidth',1)
    plot(N,bb_cpu,'rx--','markersize',24,'linewidth',1)
    plot(N,bb_gpu,'b.--','markersize',24,'linewidth',1)
    hold off
    legend('ws cpu','ws gpu','bb cpu','bb gpu')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('Number of particles')
    ylabel('time (s)')
    title('total time')
    
    %%
    subplot(1,2,2)
    plot(N,ws_cpu./ws_gpu,'bx-','markersize',24,'linewidth',1)
    hold on
    plot(N,bb_cpu./bb_gpu,'bx--','markersize',24,'linewidth',1)
    legend('ws','bb')
    hold off
    title('speedup')
    xlabel('Number of particles')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p2)
    
elseif calcResults == 2
    
    p2 = genpath(fullfile('generateResults','figure5'));
    
    addpath(p2)
    
    %% Plot
    figure(5)
    clf
    
    %%
    ws_cpu = NaN(3,1);
    load('nodeTests_cpu_1.mat')
    cpu(1) = simTime;
    
    load('nodeTests_cpu_10.mat')
    cpu(2) = simTime;
    
    load('nodeTests_cpu_100.mat')
    cpu(3) = simTime;
    
    gpu = NaN(3,1);
    load('nodeTests_gpu_1.mat')
    gpu(1) = simTime;
    
    load('nodeTests_gpu_10.mat')
    gpu(2) = simTime;
    
    load('nodeTests_gpu_100.mat')
    gpu(3) = simTime;
    
    N = [1,10,100];
    
    %%
    subplot(1,2,1)
    plot(N,cpu,'rx-','markersize',24,'linewidth',1)
    hold on
    plot(N,gpu,'b.-','markersize',24,'linewidth',1)
    hold off
    legend('cpu','gpu')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('Number of particles')
    ylabel('time (s)')
    title('total time')
    
    %%
    subplot(1,2,2)
    plot(N,cpu./gpu,'bx-','markersize',24,'linewidth',1)
    title('speedup')
    xlabel('Number of particles')
    ylabel('time (s)')
    set(gca,'XScale','log')
    
    rmpath(p2)
    
end
