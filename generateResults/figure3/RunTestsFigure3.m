function RunTestsFigure3(procFlag,nRuns)

Npts = [1,2,4,7,12,17,23];%,33];
nN = length(Npts);

timeTotal = NaN(nN,nRuns);
timeAS = NaN(nN,nRuns);
timeA = NaN(nN,nRuns);
timeBACKSLASH = NaN(nN,nRuns);


for iR = 1 : nRuns
    fprintf('\n\n%s: run %i (of %i)\n ',procFlag,iR,nRuns)
    
    for jj = 1 : nN
        
        fprintf('\t%i ',Npts(jj))
        
        %% Setup
        problemFlag = 'swimming';
        
        switch procFlag
            case 'cpu'
                blockSize = 4;
            case 'gpu'
                blockSize = [4,0];
        end
        
        swimmer = InitSwimmer(Npts(jj));
        nSw = length(swimmer);
        
        epsilon=0.25/50;
        domain='i';
        
        boundary = [];
        
        % Initial condition
        t = 0;
        z = InitCondition(swimmer);
        
        %% begin timing
        tTotal = tic;
        
        %% Get swimmer points
        [xForce,vForce,xQuad,xForceSwRotate,indSw] ...
            = GetSwimmerPoints(swimmer,z,t,'cpu');
        
        %% NN Matrices
        NNMatrices = InitialiseNNMatrices(problemFlag, ...
            swimmer,boundary,blockSize);
        
        NN = NNMatrices{1};
        NNsw = NNMatrices{2};
        
        %% Stokeslet matrix
        % Assemble matrix system - mobility problem
        NSw = length(xForce)/3;
        QSw = length(xQuad)/3;
        N = NSw;
        
        % Assemble stokeslet matrix
        tas = tic;
        switch procFlag
            case 'gpu'
                xF = gpuArray(xForce);
                xQ = gpuArray(xQuad);
                
                [AS,~]=AssembleStokesletMatrix(xF,xQ,xF,epsilon, ...
                    domain,blockSize,procFlag,NN);
            case 'cpu'
                [AS,~]=AssembleStokesletMatrix(xForce,xQuad,xForce,epsilon, ...
                    domain,blockSize,procFlag,NN);
        end
        
        
        timeAS(jj, iR) = toc(tas);
        
        %% AU block
        % component of velocity due to translational velocity of
        % swimmers;  zero velocity of boundary
        au = zeros(NSw,nSw);
        for n = 1 : nSw
            au(indSw(n):indSw(n+1)-1,n) = ...
                -ones(indSw(n+1)-indSw(n),1);
        end
        AU = kron(eye(3),au);
        
        %% AF block
        af = zeros(nSw, N);
        for n= 1 : nSw
            af(n,indSw(n):indSw(n+1)-1) = ...
                sum(NNsw(1:QSw,indSw(n):indSw(n+1)-1),1);
        end
        AF = kron(eye(3),af);
        
        %% A0m
        Ze  = zeros(N, nSw);
        x1m = zeros(N, nSw);
        x2m = zeros(N, nSw);
        x3m = zeros(N, nSw);
        
        for n = 1 : nSw
            [x1,x2,x3]=ExtractComponents(xForceSwRotate{n});
            x1m(indSw(n):indSw(n+1)-1,n) = x1;
            x2m(indSw(n):indSw(n+1)-1,n) = x2;
            x3m(indSw(n):indSw(n+1)-1,n) = x3;
        end
        AOm = [ Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];
                
        %% AM
       
        Ze  = zeros(nSw,N);
        x1m = zeros(nSw,N);
        x2m = zeros(nSw,N);
        x3m = zeros(nSw,N);
        
        [x1,x2,x3]=ExtractComponents(xQuad'*NNsw);
                
        for n=1:nSw
            x1m(n,indSw(n):indSw(n+1)-1)=x1(indSw(n):indSw(n+1)-1);
            x2m(n,indSw(n):indSw(n+1)-1)=x2(indSw(n):indSw(n+1)-1);
            x3m(n,indSw(n):indSw(n+1)-1)=x3(indSw(n):indSw(n+1)-1);
        end
        AM = [Ze -x3m x2m; x3m Ze -x1m; -x2m x1m Ze];
        
        %% A
        ta = tic;
        
        A=[ AS, AU, AOm            ; ...
            AF, zeros(3*nSw,6*nSw) ; ...
            AM, zeros(3*nSw,6*nSw)];
        
        timeA(jj, iR) = toc(ta);
        
        %% RHS
        b = [vForce ; zeros(6*nSw,1)];
        
        %% Solve problem
        tslv = tic;
        % Solve problem
        sol = A \ b;
        
        switch procFlag
            case 'gpu'
                sol = gather(sol); %#ok<NASGU>
        end
        
        timeBACKSLASH(jj, iR) = toc(tslv);
        
        %%
        timeTotal(jj, iR) = toc(tTotal);
    end
    
    %% Output
    switch procFlag
        case 'cpu'
   
            timeAScpu = mean(timeAS,2);
            timeAcpu = mean(timeA,2);
            timeBACKSLASHcpu = mean(timeBACKSLASH,2);
            
            Ncpu = Npts;
            
            save('fig3_cpu.mat','Ncpu', ...
                'timeAScpu','timeAcpu','timeBACKSLASHcpu','timeTotal')
            
        case 'gpu'
               
            timeASgpu = mean(timeAS,2);
            timeAgpu = mean(timeA,2);
            timeBACKSLASHgpu = mean(timeBACKSLASH,2);
            
            Ngpu = Npts;
            
            save('fig3_gpu.mat','Ngpu', ...
                'timeASgpu','timeAgpu','timeBACKSLASHgpu','timeTotal')
    end
    
    clearvars -except jj Npts nN iR nRuns ...
        procFlag timeAS timeA timeBACKSLASH
end

fprintf('\n')

end
