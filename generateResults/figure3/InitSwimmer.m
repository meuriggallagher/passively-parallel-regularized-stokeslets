function swimmer = InitSwimmer(nH)

nSperm = 1;
nBeats = 3;
L0 = 50; %um
bodySize = [2.0/L0, 1.6/L0, 1.0/L0];

nT = 100;

discr = [nT,nH,nT*4,nH*4];

x0 = cell(nSperm,1);
x0{1} = [ 0;   0; 0.2];
x0{2} = [-1; 0.5; 0.2];

bodyFrame = cell(nSperm,1);
bodyFrame{1} = RotationMatrix(0,3);
bodyFrame{2} = RotationMatrix(0,3);

args = cell(nSperm,1);
args{1} = struct('phase',pi/5,'k',2*pi);
args{2} = struct('phase',pi/5,'k',2*pi);

swimmer = SwimmersSperm(nSperm,@ModelSpermGI, ...
    @WaveSpermDKAct,bodySize,discr,nBeats, ...
    x0,bodyFrame,args);

end