function RunTestsFigure5

%% Load forces and cilia positions
load('fullMouseCiliaPositions.mat','X0');
x0 = X0;

nC = size(x0,1);
forceFileName = fullfile(pwd,'generateResults','figure5','ForceData',sprintf( ...
    'mouse_%iCilium.mat',nC));

%%
if ~exist('demoParticlePaths.mat','file')
    nP = 1e5;
    p0 = InitialisePointInMouseNode(nP);
    
    save('demoParticlePaths.mat','p0')
else
    load('demoParticlePaths.mat','p0')
end

%% Set run parameters
nBeats = 1000;

%% Track N particles
NP = 10.^(0:2);

for ii = 1 : length(NP)
    nP = NP(ii);

    procFlag = 'gpu';
    
    [fC,stationaryBoundary,movingBoundary, ...
        epsilon,domain,blockSize] = ParLoadForces(forceFileName);
    
    switch procFlag
        case 'cpu'
            blockSize = 4;
        case 'gpu'
            blockSize = [4,0];
            
            fC.fN = gpuArray(fC.fN);
            fC.aN = gpuArray(fC.aN);
            
            stationaryBoundary.x = gpuArray(stationaryBoundary.x);
            stationaryBoundary.X = gpuArray(stationaryBoundary.X);
    end
    
    saveDir = fullfile(pwd,'generateResults','figure5','gpuTrackData');
    
    saveName = sprintf('mouse_%iCilium_nBeats%i_nP%i.mat',nC, ...
        nBeats,nP);
    
    fprintf('Tracking %i particles:\n',nP);
    
    simTime = TrackAndTimeParticlePaths(p0,nP,fC,movingBoundary, ...
        stationaryBoundary,nBeats,epsilon,domain,blockSize,procFlag, ...
        saveDir,saveName);
    
    fprintf('\tracked %i particles in %f seconds\n',nP,simTime);
    
    saveName = sprintf('nodeTests_gpu_%i.mat',nP);
    
    save(saveName,'NP','simTime')
    

    procFlag = 'cpu';
    
    [fC,stationaryBoundary,movingBoundary, ...
        epsilon,domain,blockSize] = ParLoadForces(forceFileName);
    
    switch procFlag
        case 'cpu'
            blockSize = 4;
        case 'gpu'
            blockSize = [4,0];
            
            fC.fN = gpuArray(fC.fN);
            fC.aN = gpuArray(fC.aN);
            
            stationaryBoundary.x = gpuArray(stationaryBoundary.x);
            stationaryBoundary.X = gpuArray(stationaryBoundary.X);
    end
    
    saveDir = fullfile(pwd,'generateResults','figure5','cpuTrackData');
    
    saveName = sprintf('mouse_%iCilium_nBeats%i_nP%i.mat',nC, ...
        nBeats,nP);
    
    fprintf('Tracking %i particles:\n',nP);
    
    simTime = TrackAndTimeParticlePaths(p0,nP,fC,movingBoundary, ...
        stationaryBoundary,nBeats,epsilon,domain,blockSize,procFlag, ...
        saveDir,saveName);
    
    fprintf('\tracked %i particles in %f seconds\n',nP,simTime);
    
    saveName = sprintf('nodeTests_cpu_%i.mat',nP);
    
    save(saveName,'NP','simTime')
    
end

%%

end
