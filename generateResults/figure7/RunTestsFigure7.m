function RunTestsFigure7

%% Setup
nBeats = 15;

% Length of sperm in um
L0 = 45; 

% Range of time to simulate over
tRange = [0, 2*pi*nBeats];

% Reguarlisation parameter
epsilon=0.25/L0;

% Infinite domain = reguliarised stokeslets
domain='i';

% Use the GPU to perform calculations
procFlag = 'gpu';

% To prevent too large calculations on GPU
blockSize = [4,0];

% boundary separation 10um depth
dpth = 10 / L0;

% Create boundary as two parallel plates
boundary = struct(            ...
    'fn',   @PlaneBoundary2,  ... 
    'model', struct(          ...
    'h',    dpth,             ...
    'nx',   51,               ...
    'ny',   51,               ...
    'Nx',   204,              ...
    'Ny',   204,              ...
    'Lx',   5.5,              ...
    'Ly',   4,                ...
    'O',    [1.25;0;-dpth/2]) ...
    );

% Initialise sperm
swimmer = InitialiseManySperm(3,L0,epsilon,nBeats);

%% Align swimmers in direction of the channel

R = RotationMatrix(-2.6535,3);
for ii = 1 : numel(swimmer)
    swimmer{ii}.b10 = R(:,1);
    swimmer{ii}.b20 = R(:,2);
end

%% NN matrix
NNMatrices = InitialiseNNMatrices('swimming',swimmer, ...
    boundary,blockSize);

%% Solve the swimming problem
fprintf('Beginning swimmer simulations...\n')

odeSolTimer = tic;
vargsIn = {swimmer,boundary,NNMatrices};
sol = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
    domain,blockSize,'ode45','swimming', ...
    procFlag,'f'); % 'f' to include forces

swimmerTime = toc(odeSolTimer);
fprintf('\tSwimmer simulations completed in %f seconds\n\n',swimmerTime)

%% Initialise particle paths
x0 = linspace(0.5,3,26);
y0 = linspace(-1.5,1.5,31);
z0 = 0;
[X0,Y0,Z0] = meshgrid(x0,y0,z0);

P0 = [X0(:) ; Y0(:) ; Z0(:)];

%% Solve particle tracks
fprintf('Beginning particle track simulations...\n')

pathSolTimer = tic;
pSol = ode45(@(T,P) SolveParticlePaths(T,P,sol,swimmer,boundary, ...
    epsilon,domain,blockSize,procFlag,NNMatrices{1}), ...
    tRange,P0);
particleTime = toc(pathSolTimer);

fprintf('\Particle transport simulations completed in %f seconds\n\n', ...
    particleTime)

%% Sample particle tracks to match paper output
ts = linspace(0,15,4);
tp = ts*2*pi;

% Sample points
p = deval(pSol,tp);

% Extract x,y,z components
nP = size(p,1)/3;
px = p(       1 :   nP, :);
py = p(  np + 1 : 2*nP, :);
pz = p(2*np + 1 : 3*nP, :);

% Sample forces 
zs = deval(sol,tp);

%% Solve velocity profiles 
% Grid for calculating velocity field
nx = 55*4+1;
xv1 = linspace(-1.5,4,nx);

ny = 40*2+1;
xv2 = linspace(-2,2,ny);

[xv1,xv2] = meshgrid(xv1,xv2);

Xp = [xv1(:) ; xv2(:) ; 0*xv1(:)];

% Initialise velocity vector
U = NaN(numel(Xp),numel(tp));

% Calculate velocity
fprintf('Beginning velocity field simulations...\n')
velSolTimer = tic;
for iT = 1 : numel(tp)
    U(:,iT) = CalculateVelocity(t(iT),Xp,sol,boundary,epsilon,domain, ...
        blockSize,NNMatrices,swimmer, procFlag);
end
velocityTime = toc(velSolTimer);
fprintf('\Velocity field calculations completed in %f seconds\n\n', ...
    velocityTime)

% Extract x,y,z components
nP = size(Xp,1)/3;
ux = U(       1 :   nP, :); 
uy = U(  np + 1 : 2*nP, :); 
uz = U(2*np + 1 : 3*nP, :); 

%% Save output
save('spermParticleTracking.mat','px','py','pz','swimmer', ...
    'boundary','ts','zs','ux','uy','uz', ...
    'xv1','xv2','-v7.3')

end
