function RunTestsFigure6(procFlag)

%% Run simulations
% Number of points per swimmer
N = 2.^(3:7);

% Run simulations
TimeSimCElegans(procFlag,N);

end
