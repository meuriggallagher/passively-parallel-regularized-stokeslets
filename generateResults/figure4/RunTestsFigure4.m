function RunTestsFigure4(procFlag)

%% Run simulations
% Number of points per swimmer
NH = 3:6;

% Run simulations
TimeSimSperm(procFlag,NH);

end
