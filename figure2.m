% Figure 2
%
phi = @(r,epsi) 15*epsi^4./( 8*pi*(r.^2+epsi^2).^(7/2));

r = linspace(0,0.1,1e3);

figure(2)
clf
subplot(1,2,1)
plot(r,phi(r,0.02),'linewidth',4)
hold on
plot(r,phi(r,0.015),'linewidth',4)
plot(r,phi(r,0.01),'linewidth',4)
hold off
legend('\epsilon = 0.02','\epsilon = 0.015','\epsilon = 0.01')
ylabel('\phi_{\epsilon}(r)')
xlabel('r')

subplot(1,2,2)
plot(r,phi(r,0.02),'linewidth',4)
hold on
plot(r,phi(r,0.015),'linewidth',4)
plot(r,phi(r,0.01),'linewidth',4)
hold off
legend('\epsilon = 0.02','\epsilon = 0.015','\epsilon = 0.01')
ylabel('\phi_{\epsilon}(r)')
xlabel('r')
set(gca,'YScale','log')
